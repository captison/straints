var path = require('path');


var root = exports.root = path.join(__dirname);

exports.coverage = path.join(root, 'coverage');
exports.dist = path.join(root, 'dist');
exports.functional = path.join(root, 'test', 'specs', 'functional');
exports.lib = path.join(root, 'source', 'lib');
exports.main = path.join(root, 'source', 'main');
exports.modules = path.join(root, 'node_modules');
exports.source = path.join(root, 'source');
exports.specs = path.join(root, 'test', 'specs');
exports.test = path.join(root, 'test');
