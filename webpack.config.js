
module.exports = (env = 'dist', mode = 'none') => require(`./build/configs/${env}.config.js`)(mode)
