var jsonLoader = require('json-loader');
var yamlLoader = require('yaml-loader');


module.exports = { process: (src, file, cfg, opts) => jsonLoader(yamlLoader(src)) };
