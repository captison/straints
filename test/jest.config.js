var path = require('path');
var paths = require('../paths');
var { alias, extensions } = require('../build/build.parts');


var aliases =
{
    ...alias,
    _functional: paths.functional,
    _specs: paths.specs,
    _test: paths.test,
};

module.exports =
{
    // stop running tests after `n` failures
    bail: false,
    // globs for files to include in coverage
    collectCoverageFrom: ['source/**/*.js'],
    // directory for coverage file output
    coverageDirectory: paths.coverage,
    // module file extensions to look for (in left-to-right order)
    moduleFileExtensions: extensions.map(x => x.slice(1)),
    // map from regex module names to source paths (for webpack alias)
    moduleNameMapper: Object.entries(aliases).reduce
        ((o, [k, v]) => ({ ...o, [`^${k}$`]: v, [`^${k}(.*)$`]: `${v}$1` }), {}),
    // root directory to scan for tests
    rootDir: paths.root,
    // paths to code files that set up testing environment
    setupFiles: [path.join(paths.test, 'jest.globals.js')],
    // glob pattern to find test files
    testMatch: ['**/*.spec.js'],
    // map filename regex to paths to transformers
    transform:
    {
        '^.+\\.js$': 'babel-jest',
        '^.+\\.ya?ml$': path.join(paths.test, 'jest-support', 'yaml-transformer')
    },
    // report each individual test
    verbose: true
}
