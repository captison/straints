var fs = require('fs');
var path = require('path');
var spawn = require('cross-spawn');
var paths = require('../paths');


module.exports =
[
    {
        name: 'tests:cover',
        desc: 'Runs unit tests and generates coverage data.',
        fn: callback =>
        {
            var options = { stdio: [process.stdin, process.stdout, process.stderr] };

            var params = [`--config=${path.join(paths.test, 'jest.config.js')}`, '--no-cache', '--coverage'];

            var jest = spawn(path.join('node_modules', '.bin', 'jest'), params, options);

            jest.on('close', callback);
        }
    },
    {
        name: 'tests:exec',
        desc: 'Runs unit tests.',
        fn: callback =>
        {
            var options = { stdio: [process.stdin, process.stdout, process.stderr] };

            var params = [`--config=${path.join(paths.test, 'jest.config.js')}`, '--no-cache'];

            var jest = spawn(path.join('node_modules', '.bin', 'jest'), params, options);

            jest.on('close', callback);
        }
    },
    {
        name: 'tests:watch',
        desc: 'Runs unit tests (code rebuilt on file changes).',
        fn: callback =>
        {
            var options = { stdio: [process.stdin, process.stdout, process.stderr] };

            var params = [`--config=${path.join(paths.test, 'jest.config.js')}`, '--watch'];

            var jest = spawn(path.join('node_modules', '.bin', 'jest'), params, options);

            jest.on('close', callback);
        }
    }
]
