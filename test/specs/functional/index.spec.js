import alyze from 'alyze';
import straints from '_source';
import validator from './validator';
import * as configs from './exports';


Object.keys(configs).forEach(key =>
{
    describe(`${key} test`, () =>
    {
        // destructure test configuration data
        let { config: { contexts, ...config }, schema, fixture, expectation } = configs[key];
        // create validator
        let { validate } = straints({ schema, validator, ...config });
        // promise function to capture results
        let results = res => results = res;

        beforeAll(() => validate(fixture, contexts).then(results, results));

        it('provides a validation results object', () =>
        {
            expect(typeof results).toBe('object');
        });

        it('delivers expected overall validation result', () =>
        {
            expect(results.valid()).toBe(expectation.valid);
        });

        it('validates according to expectations', () =>
        {
            expect(results.tested).toEqual(expectation.tested);
        });
    });
});
