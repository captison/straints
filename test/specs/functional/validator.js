import alyze from 'alyze';


let impl = alyze.create({ onMissingValue: true });

impl.async =
{
    routine: (val, test, ...args) =>
    {
        return (accept, reject) => setTimeout(() => accept(impl[test](...[val].concat(args))));
    },

    promise: (...args) =>
    {
        return new Promise(impl.async.routine(...args));
    }
}

export default impl;
