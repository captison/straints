import identify from '_main/vsdTypes/identify';


describe('The identify module', () =>
{
    it('exports a default function', () =>
    {
        expect(typeof identify).toBe('function');
    });

    it('sets the identity of the config item based on path in VSD', () =>
    {
        let path = 'path.to.constraint.10';
        let item = {};
        let result = identify(path, item);

        expect(result).toBe(path);
        expect(item[C.VSD.ID]).toBe(path);
    });

    it('resolves the identity of a config item in an array using its name', () =>
    {
        let path = 'path.to.constraint.10';
        let item = { [C.VSD.NAME]: 'circular' };
        let result = identify(path, item);
        let exp = 'path.to.constraint.circular';

        expect(result).toBe(exp);
        expect(item[C.VSD.ID]).toBe(exp);
    });
});
