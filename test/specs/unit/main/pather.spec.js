import pather from '_main/pather';


describe.skip('The pather module', () =>
{
    let target =
    {

    };


    it('exports a default function', () =>
    {
        expect(typeof pather).toBe('function');
    });

    it('returns a pathing instance for a target object', () =>
    {
        let pathing = pather(target);

        expect(pathing.data).toEqual(target);
        expect(typeof pathing.get).toBe('function');
    });
});
