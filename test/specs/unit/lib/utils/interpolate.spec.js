import { interpolate } from '_lib/utils';


describe('The interpolate utility', () =>
{
    let replacements =
    {
        name: "Alpha Beta",
        city: "Sunnydale",
        emailAddress: "del.roy@lindo.biz",
        details: 'Contact the ${city} ${name} supervisor at ${emailAddress}'
    };

    it('is a function', () =>
    {
        expect(typeof interpolate).toBe('function');
    });

    it('performs simple translation', () =>
    {
        let string = 'I live in ${city} city.';
        let result = 'I live in Sunnydale city.';

        expect(interpolate(string, replacements)).toBe(result);
    });

    it('performs nested translation', () =>
    {
        let { city, name, emailAddress } = replacements;
        let string = 'She said, "${details}".';
        let result = `She said, "Contact the ${city} ${name} supervisor at ${emailAddress}".`;

        expect(interpolate(string, replacements)).toBe(result);
    });
})
