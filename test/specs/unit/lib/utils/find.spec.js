import { find } from '_lib/utils';


describe('The find utility', () =>
{
    let porders =
    [
        {
            "order_no": 982358,
            "name": "Sean",
            "address": { "city": "Polemart", "state": "OR", "zip": 83624 },
            "pizzas":
            [
                {
                    "size": "large",
                    "toppings": [ "sausage", "mushrooms", "olives", "pickles" ]
                },
                {
                    "size": "medium",
                    "toppings": [ "canadian bacon", "pineapple" ]
                }
            ]
        },
        {
            "order_no": "P-78342",
            "name": "Salma",
            "phone": "(213) 562-7140",
            "pizzas":
            [
                {
                    "size": "tiny",
                    "toppings": [ "sprouts", "onions", "spinach", "carrots" ]
                }
            ]
        }
    ]

    it('is a function', () =>
    {
        expect(typeof find).toBe('function');
    });

    it('can find root-level data', () =>
    {
        let order = porders[0];

        expect(find('order_no', order)).toBe(order.order_no);
        expect(find(['name'], order)).toBe(order.name);
    });

    it('can dive to find data', () =>
    {
        let strPath = '0.address.city';
        let arrPath = [0, 'address', 'city'];
        let value = porders[0].address.city;

        expect(find(strPath, porders)).toBe(value);
        expect(find(arrPath, porders)).toBe(value);
    });

    it('can flatten path segments to find data', () =>
    {
        let path = ['1.pizzas', 0, ['toppings', 1]];
        let value = porders[1].pizzas[0].toppings[1];

        expect(find(path, porders)).toBe(value);
    });

    it('returns null for an invalid path', () =>
    {
        let path = 'one.two.5';

        expect(find(path, porders)).toBe(null);
    });
})
