import { toString } from '_lib/utils';


describe('The toString utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof toString).toBe('function');
    });

    it('returns string parameter value as an string', () =>
    {
        let string = 'this is a string';

        expect(toString(string)).toBe(string);
    });

    it('returns a null value as an empty string', () =>
    {
        expect(toString(null)).toBe('');
    });

    it('returns an undefined value as an empty string', () =>
    {
        expect(toString(undefined)).toBe('');
    });

    it('returns an array parameter value as a single concatenated string', () =>
    {
        let result = 'a.string.of.periods';
        let value = result.split(/\./);

        expect(toString(value, '.')).toBe(result);
    });

    it('coerces a non-string, non-array parameter value into a string', () =>
    {
        let value = { this: 'is', an: /object/ };

        expect(typeof toString(value)).toBe('string');
    });
})
