import { ensure } from '_lib/utils';


describe('The ensure utility', () =>
{
    let stringValue = 'string value';
    let numberValue = 3487;
    let defaultValue = 'default value';

    it('is a function', () =>
    {
        expect(typeof ensure).toBe('function');
    });

    it('returns default value when value is undefined', () =>
    {
        expect(ensure(undefined)).toBe(undefined);
        expect(ensure(undefined, defaultValue)).toBe(defaultValue);
    });

    it('returns default value when value is null', () =>
    {
        expect(ensure(null)).toBe(undefined);
        expect(ensure(null, defaultValue)).toBe(defaultValue);
    });

    it('returns defined, non-null falsey values', () =>
    {
        expect(ensure(numberValue)).toBe(numberValue);
        expect(ensure(numberValue, defaultValue)).toBe(numberValue);
        expect(ensure('')).toBe('');
        expect(ensure('', defaultValue)).toBe('');
        expect(ensure(false)).toBe(false);
        expect(ensure(false, defaultValue)).toBe(false);
    });

    it('returns value when it exists', () =>
    {
        expect(ensure(579)).toBe(579);
        expect(ensure(579, defaultValue)).toBe(579);
        expect(ensure(stringValue)).toBe(stringValue);
        expect(ensure(stringValue, defaultValue)).toBe(stringValue);
        expect(ensure(true)).toBe(true);
        expect(ensure(true, defaultValue)).toBe(true);
    });
})
