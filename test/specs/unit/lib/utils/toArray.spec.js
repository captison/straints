import { toArray } from '_lib/utils';


describe('The toArray utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof toArray).toBe('function');
    });

    it('returns array parameter value as an array', () =>
    {
        let array = [17, { data: 'data' }, false];

        expect(toArray(array)).toEqual(array);
    });

    it('returns a null value as an empty array', () =>
    {
        expect(toArray(null)).toEqual([]);
    });

    it('returns a undefined value as an empty array', () =>
    {
        expect(toArray(undefined)).toEqual([]);
    });

    it('returns non-array parameter value as a single value array', () =>
    {
        let value = { name: "Jenny", age: 32 };
        let result = [ value ];

        expect(toArray(value)).toEqual(result);
    });

    it('returns a string parameter value as an array when delimiter is specified', () =>
    {
        let value = 'my|name|is|Jones';
        let split = /\|/;
        let result = value.split(split);

        expect(toArray(value, split)).toEqual(result);
    });

    it('returns a string parameter value as a single value array without delimiter', () =>
    {
        let value = 'my|name|is|Jones';
        let result = value.split(/\|/);

        expect(toArray(value)).not.toEqual(result);
    });
})
