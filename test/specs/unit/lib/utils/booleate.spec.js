import { booleate } from '_lib/utils';


describe('The booleate utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof booleate).toBe('function');
    });

    it('can perform the OR (||) operation', () =>
    {
        expect(booleate('or', true, true)).toBe(true);
        expect(booleate('or', true, false)).toBe(true);
        expect(booleate('or', false, true)).toBe(true);
        expect(booleate('or', false, false)).toBe(false);

        expect(booleate('||', true, true)).toBe(true);
        expect(booleate('||', true, false)).toBe(true);
        expect(booleate('||', false, true)).toBe(true);
        expect(booleate('||', false, false)).toBe(false);
    });

    it('can perform the AND (&&) operation', () =>
    {
        expect(booleate('and', true, true)).toBe(true);
        expect(booleate('and', true, false)).toBe(false);
        expect(booleate('and', false, true)).toBe(false);
        expect(booleate('and', false, false)).toBe(false);

        expect(booleate('&&', true, true)).toBe(true);
        expect(booleate('&&', true, false)).toBe(false);
        expect(booleate('&&', false, true)).toBe(false);
        expect(booleate('&&', false, false)).toBe(false);
    });

    it('can perform the XNOR (==) operation', () =>
    {
        expect(booleate('xnor', true, true)).toBe(true);
        expect(booleate('xnor', true, false)).toBe(false);
        expect(booleate('xnor', false, true)).toBe(false);
        expect(booleate('xnor', false, false)).toBe(true);

        expect(booleate('==', true, true)).toBe(true);
        expect(booleate('==', true, false)).toBe(false);
        expect(booleate('==', false, true)).toBe(false);
        expect(booleate('==', false, false)).toBe(true);
    });

    it('can perform the NOR (!|) operation', () =>
    {
        expect(booleate('nor', true, true)).toBe(false);
        expect(booleate('nor', true, false)).toBe(false);
        expect(booleate('nor', false, true)).toBe(false);
        expect(booleate('nor', false, false)).toBe(true);

        expect(booleate('!|', true, true)).toBe(false);
        expect(booleate('!|', true, false)).toBe(false);
        expect(booleate('!|', false, true)).toBe(false);
        expect(booleate('!|', false, false)).toBe(true);
    });

    it('can perform the NAND (!&) operation', () =>
    {
        expect(booleate('nand', true, true)).toBe(false);
        expect(booleate('nand', true, false)).toBe(true);
        expect(booleate('nand', false, true)).toBe(true);
        expect(booleate('nand', false, false)).toBe(true);

        expect(booleate('!&', true, true)).toBe(false);
        expect(booleate('!&', true, false)).toBe(true);
        expect(booleate('!&', false, true)).toBe(true);
        expect(booleate('!&', false, false)).toBe(true);
    });

    it('can perform the XOR (!=) operation', () =>
    {
        expect(booleate('xor', true, true)).toBe(false);
        expect(booleate('xor', true, false)).toBe(true);
        expect(booleate('xor', false, true)).toBe(true);
        expect(booleate('xor', false, false)).toBe(false);

        expect(booleate('!=', true, true)).toBe(false);
        expect(booleate('!=', true, false)).toBe(true);
        expect(booleate('!=', false, true)).toBe(true);
        expect(booleate('!=', false, false)).toBe(false);
    });
})
