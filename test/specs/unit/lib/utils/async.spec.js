import { async } from '_lib/utils';


describe('The async utility', () =>
{
    let doValueTest = value =>
    {
        let success = jest.fn(v => v);
        let failure = jest.fn(v => v);

        async(value, success, failure);

        expect(success.mock.calls.length).toBe(1);
        expect(failure.mock.calls.length).toBe(0);
        expect(success.mock.calls[0][0]).toBe(value);
    }

    it('is a function', () =>
    {
        expect(typeof async).toBe('function');
    });

    it('will pass and not fail a null value', () =>
    {
        doValueTest(null);
    });

    it('will pass and not fail an undefined value', () =>
    {
        doValueTest(undefined);
    });

    it('will pass and not fail a NaN value', () =>
    {
        doValueTest(NaN);
    });

    it('will pass and not fail a string value', () =>
    {
        doValueTest('string value');
    });

    it('will pass and not fail a number value', () =>
    {
        doValueTest(439);
    });

    it('will pass and not fail a regular expression value', () =>
    {
        doValueTest(/.*/);
    });

    it('will pass and not fail a non-thenable object value', () =>
    {
        doValueTest({ name: 'myObject' });
    });

    it('passes success and failure to a function value', () =>
    {
        let value = jest.fn((s, f) => {});
        let success = jest.fn(v => v);
        let failure = jest.fn(v => v);

        async(value, success, failure);

        expect(success.mock.calls.length).toBe(0);
        expect(failure.mock.calls.length).toBe(0);
        expect(value.mock.calls.length).toBe(1);
        expect(value.mock.calls[0][0]).toBe(success);
        expect(value.mock.calls[0][1]).toBe(failure);
    });

    it('passes success and failure to a promise or thenable object', () =>
    {
        let value = jest.fn((s, f) => {});
        let success = jest.fn(v => v);
        let failure = jest.fn(v => v);

        async({ then: value }, success, failure);

        expect(success.mock.calls.length).toBe(0);
        expect(failure.mock.calls.length).toBe(0);
        expect(value.mock.calls.length).toBe(1);
        expect(value.mock.calls[0][0]).toBe(success);
        expect(value.mock.calls[0][1]).toBe(failure);
    });
});
