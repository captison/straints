import { settle } from '_lib/utils';


describe('The settle utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof settle).toBe('function');
    });

    it('splits a string by the given regular expression', () =>
    {
        let string = 'this:=is:=a:=string';
        let split = /:=/;
        let result = string.split(split);

        expect(settle(string, split)).toEqual(result);
    });

    it('returns the string unchanged for an unmatched regular expression', () =>
    {
        let string = 'this:=is:=a:=string';
        let split = /=:/;
      
        expect(settle(string, split)).toBe(string);
    });
})
