import { push } from '_lib/utils';


describe('The push utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof push).toBe('function');
    });

    it('can create a new array with the given value', () =>
    {
        let string = 'this is a string';

        expect(push(string)).toEqual([string]);
    });

    it('can append value to an existing array', () =>
    {
        let data = ['first string'];
        let string = 'second string';
        let result = [...data, string];

        expect(push(string, data)).toEqual(result);
    });

    it('will not allow an existing value to be appended', () =>
    {
        let data = ['another string'];
        let string = 'another string';

        expect(push(string, data)).toEqual(data);
    });
})
