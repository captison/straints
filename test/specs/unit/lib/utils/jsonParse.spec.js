import { jsonParse } from '_lib/utils';


describe('The jsonParse utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof jsonParse).toBe('function');
    });

    it('returns a json object from a string value', () =>
    {
        let testValue = { name: 'Jenny', age: 32 };
        let strValue = JSON.stringify(testValue);

        expect(jsonParse(strValue)).toEqual(testValue);
    });

    it('returns string value when it is not parseable as json', () =>
    {
        let strValue = '{ name: "Jenny" age: 32 }';

        expect(jsonParse(strValue)).toBe(strValue);
    });
})
