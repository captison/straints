import { isObject } from '_lib/utils';


describe('The isObject utility', () =>
{
    it('is a function', () =>
    {
        expect(typeof isObject).toBe('function');
    });

    it('returns true when value is an object', () =>
    {
        expect(isObject({})).toBe(true);
    });

    it('returns false when value is a string', () =>
    {
        expect(isObject('string value')).toBe(false);
    });

    it('returns false when value is a number', () =>
    {
        expect(isObject(349)).toBe(false);
    });

    it('returns false when value is null', () =>
    {
        expect(isObject(null)).toBe(false);
    });

    it('returns false when value is undefined', () =>
    {
        expect(isObject(undefined)).toBe(false);
    });

    it('returns false when value is NaN', () =>
    {
        expect(isObject(NaN)).toBe(false);
    });
})
