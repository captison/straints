import { cycle } from '_lib/utils';


describe('The cycle utility', () =>
{
    let array = ['one', 'two', 'three', 'four'];

    it('is a function', () =>
    {
        expect(typeof cycle).toBe('function');
    });

    it('calls iterator function for each array item', () =>
    {
        let iter = jest.fn((next, elem, idx) => {});

        cycle(array, iter);

        expect(iter.mock.calls.length).toBe(4);
        expect(iter.mock.calls[0][1]).toBe(array[0]);
        expect(iter.mock.calls[1][1]).toBe(array[1]);
    });

    it('calls final function after all iterations have called back', () =>
    {
        let iter = next => next();
        let final = jest.fn(() => {});

        cycle(array, iter, final);

        expect(final.mock.calls.length).toBe(1);
    });

    it('does not call final function if all iterations do not call back', () =>
    {
        let iter = next => {};
        let final = jest.fn(() => {});

        cycle(array, iter, final);

        expect(final.mock.calls.length).toBe(0);
    });

    it('stops calling iterator function after the first time it return false', () =>
    {
        let iter = jest.fn((next, elem, idx) => idx < 1);

        cycle(array, iter);

        expect(iter.mock.calls.length).toBe(2);
    });
})
