import straints from '_source';


describe('The Straints package', () =>
{
    it('exports a default function', () =>
    {
        expect(typeof straints).toBe('function');
    });

    it('default function returns an instance of the validation engine', () =>
    {
        let instance = straints();

        expect(typeof instance.validate).toBe('function');
    });

    it('default function returns an instance of the validation engine with parameters', () =>
    {
        let instance = straints({ levels: 'warning', flgAllowMultiRuleKeys: false });

        expect(typeof instance.validate).toBe('function');
    });
});
