var { provide } = require('../build/build.parts')


Object.entries(provide).forEach(([key, value]) =>
{
    var [ name, ...others ] = [].concat(value);

    try
    {
        global[key] = require(name);
        while (others.length > 0) global[key] = global[key][others.shift()];
    }
    catch (e) { /* simply skipping this require */ }
});
