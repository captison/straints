var chalk = require("chalk");
var requireAll = require('require-all');


var task = process.argv[2], log = console.log, tasks = {}, charCt = 0;


/*
    Help task for info purposes.
*/
function help()
{
    log(`\n${chalk.whiteBright("Usage")}:\n`);
    log(`   ${chalk.yellowBright('node task [name]')}`);
    log(`\n${chalk.whiteBright("Available tasks")}:\n`);

    Object.keys(tasks).sort().forEach(n => log(`   ${chalk.greenBright(n.padEnd(charCt))}   ${chalk(tasks[n].desc)}`));

    log(/* empty line */);
}


/*
    Task execution.
*/
function start()
{
    var exec = name =>
    {
        if (tasks[name])
        {
            var { deps = [], fn } = tasks[name];

            [].concat(deps).forEach(exec);

            var exit = code =>
            {
                if (!code)
                    log(chalk.yellowBright(`Task ${name} Completed!`));
                else
                    process.exit(code);
            }

            if (fn) fn(exit, log);
        }
    }
    // execute the requested task
    task ? exec(task) : help();
}


/*
    The below code finds all task files in the project tree that end with
    '.tasks.js'.  A task file should export an array of objects that each
    define a single task.

    A task object should have the following:
        - name: the name of the task (required)
        - desc: a short description for the task
        - fn: the function that runs the task (required)

    Take care to use good task names to prevent collision.
*/

requireAll(
{
    dirname: __dirname,
    filter: /\.tasks\.js$/,
    excludeDirs: /^(\.git|coverage|docs|node_modules|public)$/,
    recursive: true,
    resolve: list => list.forEach(t => charCt = Math.max(charCt, (tasks[t.name] = t).name.length))
});

start();
