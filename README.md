![straints](http://straints.captison.com/images/straints-logo.png)

[![npm version](https://badge.fury.io/js/straints.svg)](https://badge.fury.io/js/straints)



Straints is a javascript object validator.  Validation rules are declaratively defined in a JSON file.

Install.

```
npm install straints --save
```

Example usage.

```js
import straints from 'straints';
import alyze from 'alyze';

let schema =
{
    "create_user":
    {
        "constrain":
        {
            "name": [ "exists" ],
            "email": [ "exists", "email" ]  
        }
    }
};

let instance = straints({ schema, validator: alyze.create() });

let target =
{
    "name": "Fred",
    "email": "fred@flintstone.com"
};

instance.validate(target, 'create_user').then(results =>
{
    if (results.valid())
        console.log('Validation Success!');
    else
        console.log('Validation Failed!');
});
```

You can find full _Straints_ documentation at the "homepage" link below.

Please use "feedback" to report any issues and "updates" for release info.


{ [homepage](http://straints.captison.com) }
{ [updates](https://bitbucket.org/captison/straints/src/master/CHANGELOG.md) }
{ [feedback](https://bitbucket.org/captison/straints/issues) }
{ [license](https://bitbucket.org/captison/straints/src/master/LICENSE) }
{ [versioning](http://semver.org/) }

Happy Validating!
