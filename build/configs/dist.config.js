var path = require('path');
var paths = require('../../paths');
var { BannerPlugin, ProvidePlugin } = require('webpack');
var { alias, banner, extensions, provide } = require('../build.parts')


module.exports = mode =>
{
    var filename = `[name]${mode === 'production' ? '.min' : ''}.js`;

    var config =
    {
        mode,

        stats: { colors: true },

        entry: { straints: path.join(paths.source, 'index.js') },

        output: { path: paths.dist, libraryTarget: 'umd', library: 'straints', libraryExport: "default", filename },

        resolve: { alias, extensions },

        module:
        {
            rules:
            [
                { test: /\.js/, exclude: paths.modules, use: 'babel-loader' },
                { test: /\.yaml/, use: ['json-loader', 'yaml-loader'] }
            ]
        },

        plugins:
        [
            new BannerPlugin({ banner, raw: false }),
            new ProvidePlugin(provide)
        ]
    }

    return config;
}
