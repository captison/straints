var webpack = require('webpack');
var webpackConfig = require('../webpack.config');


module.exports =
[
    {
        name: 'dist',
        desc: 'Builds dev & prod library distribution files.',
        deps: [ 'dist:dev', 'dist:prod' ]
    },
    {
        name: 'dist:dev',
        desc: 'Builds library distribution file.',
        fn: (callback, log) =>
        {
            var config = webpackConfig();

            webpack(config, (error, stats) =>
            {
                if(error) throw error;
                log(stats.toString(config.stats));
                callback();
            });
        }
    },
    {
        name: 'dist:prod',
        desc: 'Builds minified library distribution file.',
        fn: (callback, log) =>
        {
            var config = webpackConfig('dist', 'production');

            webpack(config, (error, stats) =>
            {
                if(error) throw error;
                log(stats.toString(config.stats));
                callback();
            });
        }
    },
    {
        name: 'dist:watch',
        desc: 'Builds library distribution file (code rebuilt on file changes).',
        fn: (callback, log) =>
        {
            var config = { ...webpackConfig(), watch: true };

            webpack(config, (error, stats) =>
            {
                if(error) throw error;
                log(stats.toString(config.stats));
            });
        }
    }
]
