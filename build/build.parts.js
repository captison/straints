var path = require('path');
var paths = require('../paths');
var packson = require('../package.json');


exports.alias =
{
    _lib: paths.lib,
    _main: paths.main,
    _source: paths.source
};

exports.extensions = ['.js', '.yaml'];

exports.provide =
{
    C: path.join(paths.lib, 'const.js'),
    utils: path.join(paths.lib, 'utils.js')
};

exports.banner = packson.name + ' v' + packson.version + ' @' + new Date().toUTCString();
