## Straints Change Log

### In Development (TODOs)


### Releases

#### 0.7.2

- fixed bug with library export in browser
- fixed bug in array constraint references
- return schema as part of validator

#### 0.7.1

- add distribution files :p

#### 0.7.0 **breaking changes**

- code refactored for ES6+
- default validator **alyze** no longer included
- removed ability to set global configuration (only global defaults remain)
- removed express support via `usage`
- bower support removed
- switched to jest for unit testing
- instance method `.validate()` now always returns a promise
- config parameters
    - `data` changed to `schema`
    - `flgAllowMultiPropKeys` and `flgAllowMultiRuleKeys` flags now default to `false`
    - `contexts` added to allow for default validation context(s)
    - `load`, `promise`, and `log` parameters removed

#### 0.6.0 **breaking changes**

- added dot-notation "pathing" for target validation objects
    - use `_` (single underscore) to reference current object
    - use `__` (double underscore) to reference parent object (when nested)
    - use `.` (single dot) to reference root object
        - start a path with a dot to get absolute path from root object
- `(t)` and `(s)` property references removed
    - use a single underscore `_` where `(t)` was used
    - use a single dot `.` where `(s)` was used
- validation method parameters
    - interpolation removed (including `t` and `s` references)
    - target values can be accessed through "pathing" (prefixed by `$`)
- 'useStrings' configuration parameter is removed

#### 0.5.3

- removed reference to strict-mode reserved word `interface`

#### 0.5.2

- fixed bug that prevented .validate() from accepting multiple contexts in comma-delimited string
- fixed bug that sometimes made duplicate constraints for test methods

#### 0.5.1 (test/documentation-only update)

- straints documentation site released
- erroneous README file gutted to contain only essential links
- testing updates for builds (Codeship), coverage (Coveralls), and grading (Codacy)

#### 0.5.0

- VSD loading
    - dropping direct support for YAML and dependency on 'js-yaml'
    - using YAML with this release has been documented in README
- configuration
    - dropped support for custom loaders (file, http)
    - 'datatype' has been removed
    - 'load' no longer accepts a string
    - there is no longer a default file that will be loaded
    - when 'load' is given a function it must use callback parameter to send VSD object
    - 'promise' is added to specify a promise implementation for .validate()
- results object
    - removed boolean properties .isValid and .isAdvised
    - removed .get() and .isValidFor()
- .validate()
    - no longer accepts a parameter to be called when validation completes
    - now returns a promise instance (if available) or a 'then' function
- bundling
    - switched from browserify to webpack
    - application entry points are now unified with UMD

#### 0.4.0

- terminology change: Validation Configuration (VC) => Validation Schema Definition (VSD)
- test method references in VSD
    - using '#' to denote the method name is no longer required
    - prefixing a path with '#' forces interpretation as test method
    - using '#' as a path separator forces interpretation as test method (backward compatibility)
- context references in VSD
    - these can now be used in rule expressions
    - prefixing a path with '@' forces interpretation as context
- results object
    - constraint identifier names for test methods are prefixed with a '#'
    - constraint identifier names for contexts are prefixed with a '@'
- can now set default property for a constraint
- condition object (include) 'if' now supports rule expressions
- properties '(t)' and '(s)' can be used to reference target and session target objects
- aggregate constraints now available
- constraint payloads
    - added 'payload' property to constraints for arbitrary data
    - payload data is available from validation results object
- added warning messages for invalid constraint and include condition attributes
- added 'param' constraint property to apply as a single parameter
- fixed bug with .cycle() that could cause stack limit exceeded issues
- constraint 'name' is no longer generated if not given
- configuration flags added
    - allow comma-delimited properties in constrain directive (flgAllowMultiPropKeys)
    - allow comma-delimited rule references in constrain directive (flgAllowMultiRuleKeys)
- simple test method parameters can now be used in rule expressions

#### 0.3.4

- 'advise' is no longer a reserved VC keyword
- fixed bug where a nested validation failure did not propagate up to top-level
- fixed regex pattern that failed to account for #'s in validation expression logic
- **alyze** is now the bundled validator
- added dist file versions that do not include js-yaml

#### 0.3.3

- refactored app logging (log setup works only from .configure() now)
- more refactoring for code conciseness
- validator object is now parsed for methods
- validator object can now be accessed from a straints instance
- 'useStrings' configuration parameter is deprecated
- results object isValid and .isValidFor() are deprecated
- results object .valid() and .validFor() have been added

#### 0.3.2

- cleaned up results data path separators
- added .findConstraints(), .findProperties(), and .isValidFor() to results object
- multiple validation levels now supported
- code is refactored to support partial context inclusion

#### 0.3.1

- minor performance improvements
- validate callback parameter info.ltarget changed to info.target
- validate callback parameter info.lname changed to info.name
- fixed a bug that prevented conditional contexts from being included
- fixed issue with include condition references

#### 0.3.0

- validator implementation methods now exposed through 'straints'
- validation rules can now be specified by constraint
- running validations
    - validate callback parameters changed
- results object
    - 'tried' and 'failed' removed
    - 'isValid' and 'isAdvised' are now booleans instead of functions
    - 'tested' now tracks the pass/fail status of each constraint test
    - .get() method to obtain errors by property
- constraints
    - 'test' has replaced 'method'
    - 'logic' has been removed
    - 'if' available for conditional constraints
- for browsers
    - 'StraintsFactory' is now just 'straints' (like in node/express usage)
- added 'advise' to context to allow defining constraints for warnings
- asynchronous validation methods now supported

#### 0.2.1

- fixed bug with non-existent validation method reference breaking the code
- fixed buggy xor logic for constraints
- added nor, nand, and xnor logic for constraints

#### 0.2.0

- **alidate** is now the bundled validator
- vc items are now referenced by array index if 'name' not given
- multiple test methods/references can now be specified in 'method' constraint property.
- constraints can now aggregate tests with 'and', 'or', or 'xor'
- constraints no longer use 'method' property as 'name' if 'name' not given
- conditional context validation now available
- property dependency validation completed

#### 0.1.0

- added support for nested validations of objects and arrays of objects

#### 0.0.4

- fixed bug with dist file creation; file sizes reduced
- fixed issue with default validator in browser

#### 0.0.3

- loading VC data now works the same in node and browser
- added, updated, and cleaned up code comments (JSDoc)

#### 0.0.2

- added bower configuration
- fixed license and changelog links in readme
- other readme updates

#### 0.0.1

- first version of **Straints**!
