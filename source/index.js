import engine from '_main/engine';


let defaultOptions =
{
    schema: null,
    levels: null,
    contexts: null,
    validator: null,
    // functionality flags
    flgAllowMultiPropKeys: false,
    flgAllowMultiRuleKeys: false
}

export default function(options)
{
    return engine({ ...defaultOptions, ...options });
}
