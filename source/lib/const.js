/*
    Constants - fixed values for the Straints application.
 */
let Constants = {};


//
// Validation Configuration Keywords
//
Constants.VSD =
{
    ALL: '____',
    ELSE: 'else',
    ID: 'path',
    IF: 'if',
    INCLUDE: 'include',
    NAME: 'name',
    NEST: 'nested',
    NOT: 'flip',
    PARAM: 'param',
    PARAMS: 'params',
    PARENT: '__',
    PAYLOAD: 'payload',
    POLL: 'poll',
    PROPERTY: 'property',
    REQUIRE: 'constrain',
    RESULTS: 'results',
    SELF: '_',
    TEST: 'test',
    THEN: 'then'
};


//
// Regular Expression Strings
//
let SRE = Constants.SRE = {};
SRE.PROP_CHARS = '[A-Za-z0-9_$]';
SRE.RULE_CHARS = '[A-Za-z0-9_$]';
SRE.ARGS_CHARS = '[^\\s]';
SRE.LOGIC = 'or|and|nor|nand|xnor|xor|not';
SRE.PROP_PRE = '(' + SRE.PROP_CHARS + '+):';
SRE.RULE_SUF = '[#@]?' + SRE.RULE_CHARS + '+([.#@]' + SRE.RULE_CHARS + '+)*';
SRE.ARGS = '[!?]' + SRE.ARGS_CHARS + '+?(:' + SRE.ARGS_CHARS + '+?)*';
SRE.RULE_PROP_REQUIRE = SRE.PROP_PRE + SRE.RULE_SUF;


//
// Regular Expression Constants
//
Constants.RE =
{
    CP_PATH_REF: /^\$[a-z0-9_.]+$/i,
    CP_REGEX_REF: /^\/.+\/[gimuy]{0,5}$/,
    GROUP: /\(\s*((not\s+)?(true|false)(\s+(or|and|nor|nand|xnor|xor)\s+(not\s+)?(true|false))*)\s*\)/ig,
    HAS_LOGIC: new RegExp('^not\\s+|\\s+(' + SRE.LOGIC + ')\\s+', 'i'),
    HAS_ARGS: new RegExp('[^\\s]+' + SRE.ARGS + '$', 'i'),
    LOGIC: new RegExp('^(' + SRE.LOGIC + '|[()])$', 'i'),
    PATH_SEP_START: /^\..+$/,
    RULE_PROP_REQUIRE: new RegExp('^' + SRE.RULE_PROP_REQUIRE + '$', 'i'),
    SEP_ITEMS: /\s*,\s*/,
    SEP_PARTS: /#/,
    SEP_PATHS: /[.]/,
    SEP_REFS: /:/,
    SEP_ARGS: /[!?]/,
};


//
// Single Character Separators
//
Constants.SEP =
{
    CTX: '@',
    ITEM: ',',
    MARG: '?',
    MTH: '#',
    PART: '#',
    PATH: '.',
    REF: ':',
    SARG: '!',
    SPC: '~'
}


//
// Context/Directive Types
//
Constants.VSD.TYPES =
{
    CONTEXT:
    [
        Constants.VSD.NAME,
        Constants.VSD.INCLUDE,
        Constants.VSD.NEST,
        Constants.VSD.REQUIRE,
    ],

    CONSTRAINT:
    [
        Constants.VSD.ID,
        Constants.VSD.NAME,
        Constants.VSD.TEST,
        Constants.VSD.IF,
        Constants.VSD.POLL,
        Constants.VSD.RESULTS,
        Constants.VSD.PARAM,
        Constants.VSD.PARAMS,
        Constants.VSD.PAYLOAD,
        Constants.VSD.PROPERTY,
        Constants.VSD.NOT
    ],

    INCLUDE:
    [
        Constants.VSD.ID,
        Constants.VSD.NAME,
        Constants.VSD.IF,
        Constants.VSD.THEN,
        Constants.VSD.ELSE
    ]
}

Constants.VSD.IS_CONTEXT =
[
    Constants.VSD.INCLUDE,
    Constants.VSD.NEST,
    Constants.VSD.REQUIRE
];

Constants.VSD.LEVELS =
[
    Constants.VSD.REQUIRE
];


//
// Log/Error Messages
//
Constants.MSG =
{
    BAD_INCLUDE_PROPERTY: '"${property}" is not a valid include condition property and will be ignored.',
    BAD_CONSTRAINT_PROPERTY: '"${property}" is not a valid constraint property and will be ignored. ' +
      'It is recommended that you use "payload" to attach arbitrary data to a constraint.',
    BAD_RULE_EXPRESSION: 'Rule expression "${value}" is not a string',
    CANNOT_RESOLVE: 'Cannot resolve ${name} item at "${path}".',
    CONTEXT_NOT_DEFINED: 'Context ${name} has not been defined.',
    EXECUTING_VALIDATIONS: 'Executing validations...',
    REQUESTING_VSD_DATA: 'Calling out for schema definition data...',
    TEST_NOT_FOUND: 'Test "${name}" was not found or its return value is not boolean.',
    WAITING_FOR_VSD_DATA: 'Waiting for schema definition data...'
}


module.exports = Constants;
