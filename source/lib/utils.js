/**
    Handles async success/failure callback routing as necessary.

    If `value` is
        - a Promise then its .then() is called with `success` and `failure`.
        - a function then it is called with `success` and `failure`.
        - anything else `success` is called with `value`.

    @param {*} value
        The target value.
    @param {function} success
        The callback to use for success.
    @param {function} failure
        The callback to use on an error.
 */
export function async(value, success, failure)
{
    // handle result as a Promise object
    if (isObject(value) && typeof value.then === 'function')
        value.then(success, failure);
    // handle result as simple async function
    else if (typeof value === 'function')
        value(success, failure);
    // assume we already have the result value
    else
        success(value);
}


/**
    Evaluates `one` and `two` as bits (0 or 1) and then performs the
    boolean `oper` between them.

    @param {string} oper
        String representation of a bitwise operator.
    @param {any} one
        The first operand.
    @param {any} two
        The second operand.
    @return {boolean}
        The result of the bitwise operation.
 */
export function booleate(oper, one, two)
{
    let res = null;

    one = one ? 1 : 0;
    two = two ? 1 : 0;

    switch (oper)
    {
        case '||': case 'or': res = one | two; break;
        case '&&': case 'and': res = one & two; break;
        case '==': case 'xnor': res = one === two; break;
        case '!|': case 'nor': res = !one & !two; break;
        case '!&': case 'nand': res = !one | !two; break;
        case '!=': case 'xor': res = one !== two; break;
    }

    return res ? true : false;
}


/**
    Loops through the items in `array`.

    For each element in `array`, `iterator` is called with the function to
    indicate completion (next), the item itself, and the index of the item.

    `callback` is called once all iterations have called next().

    @param {array} array
        Elements to be iterated.
    @param {function} iterator
        Called for each item of the array.
    @param {function} callback
        Called when the loop has completed.
 */
export function cycle(array, iterator, callback)
{
    let x = array.length, next = null;

    let cycle = () => { (x === 0 && typeof callback === 'function' && callback(), x--) }

    array.forEach((e, i) => next !== false ? (next = iterator(cycle, e, i)) : cycle());

    cycle();
}


/**
    Returns `value` only if it is neither undefined nor null.

    @param {*} value
        Value to check.
    @param {*} defaultValue
        Default value to return when `value` is not valid.
    @return {*}
        `value` or `defaultValue`.
 */
export function ensure(value, defaultValue)
{
    return typeof value === 'undefined' || value === null ? defaultValue : value;
}


/**
    Returns the item found at the given `path` in the given `data`.

    The `path` parameter can be a `sep`-delimited string or an array for
    a recursive depth search.

    @param {(string|array)} path
        Path to find in `data`.
    @param {Object} data
        Data to be searched for `path`.
    @return {*}
        Value found in `data` at `path` or null if not found.
 */
export function find(path, data)
{
    let sep = C.SEP.PATH;

    let dive = (path, data) =>
    {
        let [ name ] = path;
        // if there are no more path elements left, return `data`.
        if (typeof name === 'undefined') return data;
        // if `path` element at 0 is valid in data, keep diving.
        if (typeof data[name] !== 'undefined') return dive(path.slice(1), data[name]);
        // flatten if `path[0]` is an array
        if (Array.isArray(name)) return dive([].concat(name, path.slice(1)), data);
        // flatten if `path[0]` has multiple path elements
        if (name.indexOf(sep) >= 0) return dive(name.split(sep).concat(path.slice(1)), data);
        // we've lost our way - a `path` segment in `data` is not valid.
        return null;
    }
    // ensure we are working with an array
    if (typeof path === 'string') path = path.split(sep);

    return dive(path, data);
}


/**
    Performs value replacement in a string.

    @param {string} value
        Value to be interpolated.
    @param {object} reps
        Object containing replacement values.
    @return {string}
        Interpolated value.
 */
export function interpolate(value, reps)
{
    let ire = /\$\{(.*?)}/g;

    while (ire.test(value))
        value = value.replace(ire, (m, p) => reps[p]);

    return value;
}


/**
    Returns true if `item` is an object and is not null.

    @param {*} item
        Value to test as object.
    @return {boolean}
        Returns true if `item` is an object.
 */
export function isObject(item)
{
    return typeof item === 'object' && item !== null;
}


/**
    Returns a JSON parsed `value` or just `value` if parsing fails.

    @param {string} value
        Value to parse.
    @return {*}
        Parsed value or `value` if parsing fails.
 */
export function jsonParse(value)
{
    try
    {
        return JSON.parse(value);
    }
    catch(e)
    {
        return value;
    }
}


/**
    Adds `data` to `array` if not already added. If `array` is not provided a
    new one will be created and returned.

    @param {*} data
        Data to be added to `array`.
    @param {array} [array=[]]
        Array in which to append `data`.
    @return {array}
        Data array.
 */
export function push(data, array = [])
{
    if (array.indexOf(data) < 0)
        array.push(data);

    return array;
}


/**
    If `item` is a string and matches `split` then `split` is used to split
    `item` into an array.  Otherwise `item` is returned as-is.

    @param {string} item
        Item to be settled.
    @param {RegExp} split
        Regular Expression pattern used to split `item`.
    @return {*}
        `item` or array.
 */
export function settle(item, split)
{
    return typeof item === 'string' && split.test(item) ? toArray(item, split) : item;
}


/**
    Converts `item` into an array. An array is always returned.

    If `item` is:
      - an array: return `item`
      - a string and `split` is a RegExp: return split `item`
      - not null: return a single element array of `item`

    Return an empty array if none of the above work out.

    @param {any} item
        Item to be converted.
    @param {object} [split]
        Regular expression used to split `item` if it is a string.
    @return {array}
        The resulting array.
 */
export function toArray(item, split)
{
    if (Array.isArray(item))
        return item;
    else if (typeof item === 'string' && split instanceof RegExp)
        return item.split(split)
    else if (typeof item !== 'undefined' && item !== null)
        return [item];

    return [];
}


/**
    Converts `item` into a string. A string is always returned.

    If `item` is:
      - a string: return `item`
      - an array: convert to string delimited by `join`
      - undefined or null: return an empty string

    Return String(item) if none of the above work out.

    @param {any} item
        Item to be converted.
    @param {string} join
        String used to join `item` if it is an array.
    @return {string}
        The resulting string.
 */
export function toString(item, join = '')
{
    if (typeof item === 'string')
        return item;
    else if (Array.isArray(item))
        return item.join(join);
    else if (typeof item === 'undefined' || item === null)
        return '';

    return String(item);
}
