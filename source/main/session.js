/**
    Session - The Straints object validation session.

    Each instance of this class represents validation against a single JS
    object.

    @class
 */
import results from './results';


export default class Session
{
    /**
        Initializes a new Validation Session instance.

        @param {object} target
            Target object for this validation session.
        @param {(string|array)} rawtexts
            Comma-separated string or array of initial contexts for this
            session.
        @param {object} [parent]
            The parent validation session instance.
        @param {string} [name]
            A name for the validation session.
     */
    constructor(pather, rawtexts, parent, name)
    {
        this.pather = pather;
        // the session target object
        this.target = utils.ensure(pather.data, {});
        // all property names for the target object
        this.fields = Object.keys(this.target);
        // initial contexts for this session
        this.rawtexts = rawtexts;
        // the parent session of this instance.
        this.parent = parent || {};
        // the namespace for this instance
        this.name = name;
        // validation session results
        this.results = this.parent.results || results(this.target);
        // constraint tests, session level contexts, nested property contexts
        this.tested = {}, this.contexts = [], this.nesteds = {};
        // copy the validate callback from the parent (if there is one)
        this.validateCB = this.parent.validateCB;
    }

    /**
        Returns [part] prefixed with the name of this session object.

        @private

        @param {string} part
            The name to prefix.
        @param {string} [separator=C.SEP.PATH]
            The separator to use between this session name and [part].
        @return {string}
            The namespaced value for [part].
     */
    namespace(part, separator)
    {
        return (this.name ? this.name + (separator || C.SEP.PATH) : '') + part;
    }

    nsRef(prop, rule)
    {
        return rule.split(C.SEP.REF).length > 1 ? rule : [this.namespace(prop), rule].join(C.SEP.REF);
    }

    /**
        Returns the validation result for a given test.

        @param {string} property
            The property of the target object.
        @param {string} path
            The constraint identifier.
        @return {boolean}
            True or false if the test has been executed; null otherwise.
     */
    resultFor(property, path)
    {
        return this.results.tests[this.nsRef(property, path)];
    }

    /**
        Returns true if [context] is a recorded context for this session.

        @param {string} context
            The context to check for.
        @return {boolean}
            True if [context] is a recorded context for this session.
     */
    hasContext(context, directive)
    {
        return (this.contexts.indexOf(context) >= 0) ||
            (directive && this.contexts.indexOf(context + C.SEP.PART + directive) >= 0);
    }

    /**
        Get or set arbitrary data in the current object.  If [value] is not
        provided, then this method behaves like a getter.

        @param {string} name
            The name of the information to set or get.
        @param {*} [value]
            The information to set.
        @return
            The information requested (getter) or nothing (setter).
     */
    inform(name, value)
    {
        if (typeof value !== 'undefined')
            this[name] = value;
        else
            return this[name];
    }

    /**
        Adds [contexts] to this session and the internal results object.

        @param {(string|array)} context
            The context to add.
        @return {boolean}
            True if the context was successfully added.
     */
    addContext(context, directive)
    {
        let missing = !this.hasContext(context, directive);

        if (missing)
        {
            // if no directive was given then remove all context references
            if (!directive)
            {
                this.contexts = this.contexts.filter(c => c.indexOf(context) !== 0);
            }
            // session: add the context if not already added
            utils.push(context + (directive ? C.SEP.PART + directive : ''), this.contexts);
        }

        return missing;
    }

    /**
        Creates a child object validation session for [property].

        @param {string} property
            The name of the property to create a child session for.
        @return {object}
            A new session instance if [property] is a child object.
     */
    nestedSession(property)
    {
        if (this.nesteds[property])
            return new Session(this.pather.get(property), this.nesteds[property], this, this.namespace(property));
    }

    /**
        Adds a nested context for [properties] if they are objects or arrays in
        `this.target`.

        @param {array} properties
            The list of properties to be nested.
        @param {string} prefix
            The path prefix for the nested properties.
        @return {object}
            Self.
     */
    addNestedContext(property, prefix)
    {
        let add = (suffix, property) =>
        {
            if (Array.isArray(this.target[property]) || utils.isObject(this.target[property]))
                (this.nesteds[property] = this.nesteds[property] || []).push(prefix + C.SEP.PATH + suffix);
        }
        // 'C.VSD.ALL' means the context should apply for every nested object
        if (property === C.VSD.ALL && !this.target.hasOwnProperty(C.VSD.ALL))
            this.fields.forEach(f => add(C.VSD.ALL, f));
        else
            add(property, property);

        return this;
    }

    /**
        Indexes [constraint] and records its attempt to validate [property].

        @param {string} property
            The name of the property being validated.
        @param {object} constraint
            The constraint representing the validation test.
        @param {string} level
            The validation level.
        @return {object}
            Self.
     */
    addTest(property, constraint, level)
    {
        let add = prop =>
        {
            // get the extended name for [property].
            let name = this.namespace(prop);
            let path = constraint[C.VSD.ID];

            this.results.tested[level] = this.results.tested[level] || {};
            this.results.tested[level][name] = this.results.tested[level][name] || {};
            // results: add the constraint id for the property if not already added
            if (typeof this.results.tested[level][name][path] !== 'boolean')
                this.results.tested[level][name][path] = null;
            // results: capture the constraint object if not already captured
            if (!this.results.constraints[path])
                this.results.constraints[path] = constraint;

            this.tested[level] = this.tested[level] || {};
            // session: add the constraint id for the property if not already added
            this.tested[level][prop] = utils.push(path, this.tested[level][prop]);
        }
        // 'C.VSD.ALL' means the constraint validation will apply to all properties
        if (property === C.VSD.ALL && !this.target.hasOwnProperty(C.VSD.ALL))
            this.fields.forEach(add);
        else
            add(property);

        return this;
    }

    /**
        Commences the session.

        This method should be called only on the root session instance.
     */
    commence()
    {
        this.commenceAt = new Date();
        return this;
    }

    /*
        Completes some last-minute tasks for the session including recording
        validation execution time.
     */
    finishUp()
    {
        // add all contexts from this session to [this.results];
        this.contexts.forEach(c => this.results.contexts.push(this.namespace(c, C.SEP.REF)));

        this.completeAt = new Date();
        // record execution time
        this.results.timeMillis = this.completeAt.getTime() - this.commenceAt.getTime();
    }

    /**
        Completes the session successfully and executes the completeCB callback
        if it exists.

        This method should be called only on the root session instance.
     */
    complete()
    {
        this.finishUp();

        this.results.isComplete = true;
        // send complete callback if given
        if (typeof this.completeCB === 'function') this.completeCB(this.results);

        return this;
    }

    /**
        Fails the session miserably and executes the miscarryCB callback if it
        exists.

        This method should be called only on the root session instance.

        @param {string} error
            The validation error that occurred.
     */
    miscarry(error)
    {
        this.finishUp();

        this.results.isComplete = false;

        this.results.error = error;
        // send complete callback if given
        if (typeof this.miscarryCB === 'function') this.miscarryCB(this.results);

        return this;
    }

    /**
        Invokes the validate callback (if available) with the namespaced
        property being validated, the full target object, [constraint], and
        [result].

        @param {string} property
            Name of the property being validated.
        @param {object} path
            The identifier of the validating constraint.
        @param {boolean} result
            The result of [constraint] validating [property].
        @param {string} type
            The validation level.
        @return {boolean}
            [result] or the return value of the callback.
     */
    validate(property, path, result, level)
    {
        // get the extended name for [property]
        let name = this.namespace(property);
        // track result before making the validate callback
        this.results.tests[this.nsRef(property, path)] = result;
        // possible we get no validation result on failed condition
        if (result !== null)
        {
            // send validate callback if available
            if (typeof this.validateCB === 'function')
            {
                let validationInfo =
                {
                    // the local name of the property validated
                    name: property,
                    // the session name of the property validated
                    sname: name,
                    // the object whose property is being validated
                    target: this.target,
                    // the original validation target
                    starget: this.results.target,
                    // the validation rule executed
                    rule: this.results.constraints[path],
                    // the validation type
                    level: level,
                    /** @deprecated (v0.3.1) */
                    type: level
                };
                // validate callback updates result;
                result = this.validateCB(result, validationInfo);
            }
        }
        // track this result in results object and return
        return this.results.tested[level][name][path] = result;
    }
}
