import resolve from './resolve';


export function baseType(data, mark)
{
    let cache = {};

    let face =
    {
        data,
        // default add to cache method
        cacheSet: function(name, item) { cache[this.markup(name)] = item; return name; },
        // default get from cache method
        cacheGet: function(name) { return cache[this.markup(name)]; },
        // default get method
        get: function(name) { return this.ref(name) ? this.cacheGet(name) : null; },
        // default finder method
        find: function(name) { return utils.find(this.unmark(name), data); },
        // item is marked?
        // marked: function(n) { return mark ? n.slice(0, 1) === mark : false },
        // mark item for caching (reference)
        markup: function(n) { return mark ? this.nomark(n.slice(0, 1) === mark ? n : mark + n) : n; },
        // unmark item for search (definition)
        unmark: function(n) { return mark ? this.nomark(n.slice(0, 1) === mark ? n.slice(1) : n) : n; },
        // NOTE: for backward compatibility
        nomark: function(n) { return mark ? n.charAt(0) + n.slice(1).replace(mark, C.SEP.PATH) : n; }
    }

    return face;
}

export function resolvedType(data, mark)
{
    var face =
    {
        ...baseType(data, mark),

        resolve: function(path, list) { return resolve(path, this, list); },

        ref: function(value)
        {
            // return if constraint item is cached
            if (this.cacheGet(value)) return true;
            // try to find the constraint
            var item = this.find(value);
            // cache the constraint if valid
            return (this.is(item) && this.cacheSet(value, item)) ? true : false;
        },

        get: function(name)
        {
            return (this.ref(name) || this.resolve(name).indexOf(name) >= 0) ? this.cacheGet(name) : null;
        },

        check: function(item, retval)
        {
            let tn = this.name.toUpperCase();
            // limit this check's interference with processing
            setTimeout(function()
            {
                utils.cycle(Object.keys(item), (next, key) =>
                {
                    let keyIsGood = C.VSD.TYPES[tn].indexOf(key) >= 0;

                    // if (!keyIsGood) log.inter('warn', C.MSG['BAD_' + tn + '_PROPERTY'], { property: key });

                    return keyIsGood;
                });
            });

            return retval;
        }
    };

    return face;
}
