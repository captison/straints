/*
    VC Items -
 */
import Session from '_main/session';
import { baseType, resolvedType } from './bases';
import identify from './identify';


export default function(config)
{
    let types = {};
    // all valid directives for a context
    let directives = [].concat(C.VSD.IS_CONTEXT, config.levels);

    types.contexts =
    {
        ...baseType(config.schema, C.SEP.CTX),

        name: 'context',

        is: function(value)
        {
            if (utils.isObject(value))
            {
                for (var key in value)
                {
                    if (value.hasOwnProperty(key) && this.directive(key)) return true;
                }
            }

            return false;
        },

        ref: function(value)
        {
            let idx = value.indexOf(C.SEP.PART);
            // determine context name
            let context = idx < 0 ? value : value.slice(0, idx);
            // determine context directive name if given
            let directive = idx < 0 ? null : value.slice(idx + 1);
            // get the context item
            let item = this.cacheGet(context);
            // find context if necessary
            let valid = item || this.is(item = this.find(context));
            // cache the context if valid
            if (valid) this.cacheSet(context, item);
            // context must exist and directive must be absent or valid
            return valid && (!directive || this.directive(directive));
        },

        directive: function(value)
        {
            return directives.indexOf(value) >= 0;
        },

        exec: function(name, value)
        {
            return (success, failure) =>
            {
                let session = new Session(value, this.unmark(name));
                // completed validation sends success callback with results
                let results = () => { success(session.results.valid()); }
                // use a separate validation session to test condition
                utils.async(config.engine.commence(session), results, failure);
            }
        }
    };

    types.methods =
    {
        ...baseType(config.validator, C.SEP.MTH),

        name: 'method',

        is: function(value)
        {
            return typeof value === 'function';
        },

        ref: function(value)
        {
            // return if method item is cached
            if (this.cacheGet(value)) return true;
            // try to find the method
            let item = this.find(value);
            // cache the method if valid
            return (this.is(item) && this.cacheSet(value, this.pack(value, item))) ? true : false;
        },

        pack: function(path, item)
        {
            let name = this.unmark(path);
            let idx = name.lastIndexOf(C.SEP.PATH);
            let owner = idx < 0 ? this.data : this.find(name.slice(0, idx));

            return { func: item, owner: owner };
        },

        exec: function(name, value, params)
        {
            let test = this.get(name);
            return test ? test.func.apply(test.owner, [value.data].concat(params)) : null;
        }
    };

    types.constraints =
    {
        ...resolvedType(config.schema),

        name: 'constraint',

        is: function(value)
        {
            if (!utils.isObject(value)) return false;
            // regular or aggregate constraint
            if (value[C.VSD.TEST] || value[C.VSD.POLL]) return this.check(value, true);

            return false;
        },

        translate: function(name, item)
        {
            let marker = this.markup(name);
            // already cached so good to go
            if (this.cacheGet(marker)) return marker;
            // item is a constraint
            if (this.is(item)) return this.cacheSet(identify(marker, item), item);
            // assume rule expression if logic gates in string
            if (typeof item === 'string' && C.RE.HAS_LOGIC.test(item) || C.RE.HAS_ARGS.test(item))
            {
                let newItem = { [C.VSD.TEST]: item };
                return this.cacheSet(identify(name, newItem), newItem);
            }
            // item is a rule reference
            if (typeof item === 'string') return this.translateString(item);
            // item is null but name is a rule reference
            if (typeof name === 'string' && item === null) return this.translateString(name);
        },

        translateString: function(string)
        {
            let idx = string.indexOf(C.SEP.REF) + 1;
            let test = string.slice(idx);
            let pre = string.slice(0, idx);

            let set = type =>
            {
                let rule = pre + type.markup(test);
                let newItem = { [C.VSD.TEST]: rule };
                return this.cacheSet(identify(rule, newItem), newItem);
            }
            // string is a constraint reference
            if (this.ref(test)) return this.markup(test);
            // string is a method reference
            if (types.methods.ref(test)) return set(types.methods);
            // string is a context reference
            if (types.contexts.ref(test)) return set(types.contexts);
            // string is a property rule
            if (C.RE.RULE_PROP_REQUIRE.test(string))
            {
                let newItem = { [C.VSD.TEST]: string };
                return this.cacheSet(identify(string, newItem), newItem);
            }
        }
    };

    types.includes =
    {
        ...resolvedType(config.schema),

        name: 'include',

        is: function(value)
        {
            if (!utils.isObject(value)) return false;

            if (value[C.VSD.THEN] || value[C.VSD.ELSE]) return this.check(value, true);

            return false;
        },

        translate: function(name, item)
        {
            let marker = this.markup(name);
            // already cached so good to go
            if (this.cacheGet(marker)) return marker;
            // item is a condition
            if (this.is(item)) return this.cacheSet(identify(marker, item), item);

            if (typeof item === 'string')
            {
                let set = type =>
                {
                    let rule = type.markup(item);
                    let newItem = { [C.VSD.THEN]: rule };
                    return this.cacheSet(identify(rule, newItem), newItem);
                }
                // item is a condition reference
                if (this.ref(item)) return this.markup(item);
                // item is a context reference
                if (types.contexts.ref(item)) return set(types.contexts);
            }
        }
    };

    return types;
}
