
/**
    Resolves the `type` path names for the item(s) found at `path`.

    @param {(array|string)} path
        Array or string path to a single `type` definition or array of
        the same in the VSD.
    @param {object} type
        Schema type of validation config item being searched for.
    @return {array}
        List of items by name resolved from `path`.
 */
export default function resolve(path, type, list)
{
    list = list || [];

    let parentOwned = path =>
    {
        let idx = path.lastIndexOf(C.SEP.PATH);
        return idx > 0 && resolve(path.slice(0, idx), type).indexOf(path) >= 0;
    }

    let solve = (path, item) =>
    {
        let temp = null;
        // settle single item or array
        item = utils.settle(item || type.find(path), C.RE.SEP_ITEMS);
        // check if item can be resolved from parent item
        if (item === null && parentOwned(path))
            utils.push(path, list);
        // when item is an array loop it
        else if (Array.isArray(item))
            item.forEach((t,i) => solve([path, i].join(C.SEP.PATH), t));
        // capture item if it can be translated
        else if (temp = type.translate(path, item))
            utils.push(temp, list);
        //
        else if (typeof item === 'string')
            solve(item);
        // at a loss here so log a warning
        // else
        //     log.inter('warn', C.MSG.CANNOT_RESOLVE, { name: type.name, path: path });
    }

    solve(utils.toString(path, C.SEP.PATH));

    return list;
}
