
/**
    Returns an identifier for `item`. If `path` ends with a number
    (array index) and `item` has a `VSD.NAME` property, then the last path
    segment will be dropped and the name appended. This makes referencing
    items from within the VC a little easier.

    For example, a constraint named 'required' could change path like so:
        'path.to.constraint.10'  =>  'path.to.constraint.required'

    @param {(string|array)} path
        Dot-delimited string or array base path for `item`.
    @param {object} item
        Validation config item object to resolve the path for.
    @return {string}
        Resolved path for `item`.
 */
export default function(path, item)
{
    path = utils.toArray(path, C.RE.SEP_PATHS);
    // pop if [item] has a name and last path item is a number
    if (item[C.VSD.NAME] && /^[0-9]+$/.test(path[path.length-1]))
    {
        path.pop();
        path.push(item[C.VSD.NAME]);
    }
    // set the item ID to the full path and return
    return item[C.VSD.ID] = utils.toString(path, C.SEP.PATH);
}
