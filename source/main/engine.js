import pather from './pather';
import vsdRules from './vsdRules';
import vsdTypes from './vsdTypes';
import Session from './session';


/**
    Engine - Parses the validation configuration and executes validations.
 */
export default function(options)
{
    let find = path => utils.find(path, schema)


    /**
        Validates the [target] object against the specified [contexts].

        The [validate] callback should return [result] or the boolean result of
        any additional processing needed.

        @param {object} target
            The object whose properties are to be validated.
        @param {(string|array)} contexts
            The comma-delimited string or array of contexts to validate with.
        @param {function} validate
            The method to be called after each validation test.
        @return {Promise}
            Resolves or rejects with a validation results object.
     */
    let validate = (object, contexts, validate) =>
    {
        return new Promise((success, failure) =>
        {
            let session = new Session(pather(object), contexts || config.contexts);
            // configure the validaton session callbacks
            session.inform('completeCB', success);
            session.inform('miscarryCB', failure);
            session.inform('validateCB', validate);
            // begin validation
            commence(session)();
        });
    }


    /**
        Extension of .kickoff() to call .perform() to begin validation.

        @param {object} session
            The validation session object.
        @return {function}
            Async success/failure callback router.
     */
    let commence = session =>
    {
        return (success, failure) =>
        {
            session.commence();
            // success/failure callbacks
            let toSuccess = () => { session.complete(); if (success) success(); }
            let toFailure = e => { session.miscarry(e); if (failure) failure(e); }
            // start validating!
            utils.async(perform(session), toSuccess, toFailure);
        }
    }


    /**
        Starting point for validation. Validation configuration data should
        already have been loaded.

        @param {object} session
            The validation session object.
        @return {function}
            Async success/failure callback router.
     */
    let perform = session =>
    {
        return (success, failure) =>
        {
            let work = () =>
            {
                // execute constraint validations
                let cycle = (next, level) => utils.async(execute(session, level), next, failure)
                // callback for end of cycle.
                let finish = () =>
                {
                    let nesteds = (next, key) => utils.async(commence(session.nestedSession(key)), next, failure)
                    // loop through session nested keys
                    utils.cycle(Object.keys(session.inform('nesteds')), nesteds, success);
                }
                // cycle validation rule types then recursively handle nested elements
                utils.cycle(config.levels, cycle, finish);
            }
            // expand the session contexts prior to validaton
            utils.async(prepare(session, session.inform('rawtexts')), work, failure);
        }
    }


    /**

        @param {object} session
            The validation session object.
        @return {function}
            Async success/failure callback router.
     */
    let prepare = (session, contexts) =>
    {
        return (success, failure) =>
        {
            utils.cycle(utils.toArray(contexts, C.RE.SEP_ITEMS), (next, context) =>
            {
                // split 'context' into name and directive
                let [ name, directive = null ] = types.contexts.unmark(context).split(C.SEP.PART, 2);

                let contextObj = types.contexts.get(name);
                // make sure the context exists
                if (!contextObj) return next();
                // add the context to the session or next() if not successful
                if (!session.addContext(name, directive)) return next();
                // capture whether or not we are including the entire context
                let doApply = d => typeof contextObj[d] !== 'undefined' && (directive === null || directive === d);

                let prefix = name + C.SEP.PATH;

                config.levels.forEach(level =>
                {
                    if (doApply(level))
                        applyConstrainDirective(session, prefix + level, level);
                });

                if (doApply(C.VSD.NEST))
                    applyNestedDirective(session, prefix + C.VSD.NEST);

                if (doApply(C.VSD.INCLUDE))
                    utils.async(applyIncludeDirective(session, prefix + C.VSD.INCLUDE), next, failure);
                else
                    next();

            }, success);
        }
    }

    /**
        Adds constraints identified by [cnames] to [session] for [property] at
        the specified validation [level].

        @param {object} session
            The validation session object.
        @param {string} property
            The property to have constraints added.
        @param {array} cnames
            The constraint identifiers to add.
        @return {object}
            The validation session object.
     */
    let applyConstraints = (session, property, cnames, level) =>
    {
        cnames.forEach(name => session.addTest(property, types.constraints.get(name), level));
    }


    /**
        Applies the constrain directive specified by [path] to the session.

        @param {object} session
            The validation session object.
        @param {string} path
            VSD path to a constrain directive.
        @param {string} level
            The validation level.
        @return {object}
            The validation session object.
     */
    let applyConstrainDirective = (session, path, level) =>
    {
        Object.keys(find(path) || {}).forEach(property =>
        {
            let extPath = [].concat(path, property);
            // properties by rule reference specified
            if (property.slice(0, 1) === C.SEP.SPC)
            {
                let cnames = [], cref = property.slice(1);
                // split by commas if flag set
                let crefs = config.flgAllowMultiRuleKeys ? utils.toArray(cref, C.RE.SEP_ITEMS) : [ cref ];
                // resolve all constraint rule references
                crefs.forEach(item => types.constraints.resolve(item, cnames));
                // loop through the property names specified for this constraint
                utils.toArray(find(extPath), C.RE.SEP_ITEMS).forEach(item =>
                {
                    applyConstraints(session, item, cnames, level);
                });
            }
            // constraints by property name specified
            else
            {
                let cnames = types.constraints.resolve(extPath);
                // split by commas if flag set
                let props = config.flgAllowMultiPropKeys ? utils.toArray(property, C.RE.SEP_ITEMS) : [ property ];
                // usually a single property but allow comma-delimited set
                props.forEach(prop => applyConstraints(session, prop, cnames, level));
            }
        });
    }


    /**
        Applies the include directive specified by [path] to the session.

        @private

        @param {object} session
            The validation session object.
        @param {string} path
            VSD path to a include directive.
        @return {function}
            Async success/failure callback router.
     */
    let applyIncludeDirective = (session, path) =>
    {
        return (success, failure) =>
        {
            utils.cycle(types.includes.resolve(path), (next, name) =>
            {
                let include = types.includes.get(name);

                let conditions = include[C.VSD.IF];
                // if this include has an 'if' we must test it
                if (conditions)
                {
                    // assume a list of contexts if conditions is an array
                    let expression = Array.isArray(conditions) ? conditions.join(' and ') : conditions;
                    // use a separate validation session to test expression
                    let result = rules.expression(expression)(C.VSD.SELF, session.pather);

                    let doInclude = result =>
                    {
                        utils.async(prepare(session, include[result ? C.VSD.THEN : C.VSD.ELSE]), next, failure);
                    }

                    utils.async(result, doInclude, failure);
                }
                else
                {
                    utils.async(prepare(session, include[C.VSD.THEN]), next, failure);
                }

            }, success);
        }
    }


    /**
        Applies the nested directive specified by `path` to the session.

        @private

        @param {object} session
            The validation session object.
        @param {string} path
            VSD path to a nested directive.
        @return {object}
            The validation session object.
     */
    let applyNestedDirective = (session, path) =>
    {
        let strPath = utils.toString(path, C.SEP.PATH);
        // add nested items to session
        Object.keys(find(path)).forEach(property => session.addNestedContext(property, strPath));
    }


    /**
        Executes validation for the session.

        @param {object} session
            The validation session object.
        @param {string} level
            The validation level.
        @return {function}
            Async success/failure callback router.
     */
    let execute = (session, level) =>
    {
        return (success, failure) =>
        {
            // get details from the session
            let tested = session.inform('tested')[level] || {};
            // cycle through all properties and constraints
            utils.cycle(Object.keys(tested), (next, property) =>
            {
                utils.cycle(tested[property], (next, name) =>
                {
                    // check for a result already in the session, and...
                    let bool = session.resultFor(property, name);
                    // do not execute the rule if we have already done so
                    let result = typeof bool === 'boolean' ? bool : rules.constraint(name)(property, session.pather);
                    // update session validation; move to next constraint
                    utils.async(result, res => { session.validate(property, name, res, level); next(); }, failure);
                // start next property cycle
                }, next);
            // execute success when all properties cycled
            }, success);
        }
    }

    let { schema, validator } = options;

    let config =
    {
        ...options,
        levels: [].concat(C.VSD.LEVELS, utils.toArray(options.levels || [], C.RE.SEP_ITEMS)),
        engine: { commence, validator }
    }

    let types = vsdTypes(config);
    let rules = vsdRules(types);

    return { schema, validate, validator };
}
