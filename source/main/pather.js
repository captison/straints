
export default function(root)
{
    let cache = {};

    let target = path =>
    {
        let face =
        {
            data: utils.find(path, root),

            get: subPath =>
            {
                if (typeof subPath === 'string')
                {
                    // single char split on single char `subPath` to help capture root path
                    subPath = subPath.length > 0 ? subPath.split(C.SEP.PATH, subPath.length === 1 ? 1 : void 0) : [];
                }
                // assume absolute path when first element is empty string
                let usePath = subPath[0] === '' ? subPath.slice(1) : path.concat(subPath);

                let idx = null;
                // allow for self path navigation
                while ((idx = usePath.indexOf(C.VSD.SELF)) >= 0) usePath.splice(idx, 1);
                // allow for parent path navigation
                while ((idx = usePath.indexOf(C.VSD.PARENT)) >= 0) usePath.splice(idx - 1, 2);

                let key = usePath.length === 0 ? 'root' : usePath.join(C.SEP.PATH);

                return cache[key] = cache[key] || target(usePath);
            }
        }

        return face;
    }

    return cache['root'] = target([]);
}
