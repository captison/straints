
export default function()
{
    let data =
    {
        // overall validity
        valid: true,
        // number of tests that passed
        passCount: 0,
        // number of tests that failed
        failCount: 0,
        // total number of tests
        testCount: 0,
        // property names that passed tests
        passed: [],
        // property names that failed tests
        failed: [],
        // all property names tested
        tested: []
    }

    let aggregator =
    {
        data,

        update: (key, result) =>
        {
            data.tested.push(key); data.testCount += 1;

            if (result === true) { data.passed.push(key); data.passCount += 1; }
            if (result === false) { data.failed.push(key); data.failCount += 1; }
            // if (result === null) { data.skipped.push(key); data.skipCount += 1; }
            data.valid = data.valid && result;
        }
    }

    return aggregator;
}
