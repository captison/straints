import pather from '_main/pather';
import compile from './compile';
import evaluate from './evaluate';
import aggregator from './aggregator';


/*
    Rules - Manages asynchronous execution of validation rules.
*/
export default function(types)
{
    let rule = constraint =>
    {
        return (property, target) =>
        {
            // reset default property if specified in constraint
            property = utils.ensure(constraint[C.VSD.PROPERTY], property);

            let ruleargs = argStr =>
            {
                let param = constraint[C.VSD.PARAM], params = constraint[C.VSD.PARAMS];
                // determine single vs. multi arg parameters
                let args = typeof param !== 'undefined' ? [ param ] : params;
                // args provided here override those in constraint
                if (typeof argStr === 'string' && argStr.length > 0)
                {
                    let form = argStr.slice(0, 1);
                    let argArr = argStr.slice(1).split(C.RE.SEP_REFS);
                    // all params are strings here so try to parse for literals
                    argArr = argArr.map(utils.jsonParse);
                    // determine single vs. multi arg parameters
                    args = form === C.SEP.SARG ? [ argArr ] : argArr
                }

                return args;
            }

            let ruledata = rule =>
            {
                // split one separates rule from parameters
                let sone = rule.split(C.RE.SEP_ARGS, 2);
                // initial part is 'property:test'
                let pref = sone.length > 1 ? sone[0] : rule;
                // get param string for the test if any
                let args = sone.length > 1 ? rule.slice(pref.length) : '';
                // split two separates property from test
                let stwo = pref.split(C.RE.SEP_REFS, 2);
                // either 'test' or 'property:test' has been specified
                let test = stwo.length > 1 ? stwo[1] : stwo[0];
                // use the current property none specified
                let prop = stwo.length > 1 ? stwo[0] : property;

                return { test: test, args: args, prop: prop, value: target.get(prop) };
            }

            let ruleexec = vals =>
            {
                // `test` is resolved as a validator method
                if (types.methods.ref(vals.test))
                    return types.methods.exec(vals.test, vals.value, compile(ruleargs(vals.args), target));
                // `test` is resolved as a context reference
                if (types.contexts.ref(vals.test))
                    return types.contexts.exec(vals.test, vals.value);
                // `test` is resolved as a constraint
                if (constraint[C.VSD.ID] !== vals.test && types.constraints.get(vals.test))
                    return entry.constraint(vals.test)(vals.prop, target);

                return null;
            }

            let invoke = rule =>
            {
                return (success, failure) =>
                {
                    let rdata = ruledata(rule);

                    let checker = result =>
                    {
                        typeof result === 'boolean' ? success(result) :
                            failure(utils.interpolate(C.MSG.TEST_NOT_FOUND, { name: rdata.test }));
                    }
                    // handle rule results
                    utils.async(ruleexec(rdata), checker, failure);
                }
            }

            return (success, failure) =>
            {
                // function to call success with result flipped if necessary.
                let final = res => { success(constraint[C.VSD.NOT] === true ? !res : res); }
                // runs rule expression
                let runner = (e, s) => utils.async(evaluate(e, invoke), s, failure)
                // handle constraint value aggregation
                let aggregate = () =>
                {
                    let subTarget = target.get(property).data;
                    // object to hold aggregate result data
                    let gator = aggregator();
                    // to loop through object keys
                    let cycle = (next, key) =>
                    {
                        let aggregate = result => { gator.update(key, result); next(); }
                        let rule = entry.expression(constraint[C.VSD.POLL], constraint[C.VSD.PARAMS]);
                        utils.async(rule(C.VSD.SELF, pather(subTarget[key])), aggregate, failure);
                    }
                    // executed when object key loop completed
                    let finish = () =>
                    {
                        let result = gator.data.valid;
                        // test aggregate data against expression if present
                        if (constraint[C.VSD.RESULTS])
                            result = entry.expression(constraint[C.VSD.RESULTS])(C.VSD.SELF, pather(gator.data));

                        utils.async(result, final, failure);
                    }

                    utils.cycle(Object.keys(subTarget || {}), cycle, finish);
                }
                // run the proper constraint test based on constraint type
                let select = res =>
                {
                    if (res)
                        constraint[C.VSD.TEST] ? runner(constraint[C.VSD.TEST], final) : aggregate();
                    else
                        success(null);
                }

                if (constraint[C.VSD.IF])
                    runner(constraint[C.VSD.IF], select);
                else
                    select(true);
            }
        }
    }
    // cache for generated rule functions
    let cache = {};
    // rule execution entry points
    let entry =
    {
        expression: (e, p) => rule({ [C.VSD.TEST]: e, [C.VSD.ID]: '_', [C.VSD.NAME]: e, [C.VSD.PARAMS]: p }),

        constraint: cname => cache[cname] = cache[cname] || rule(types.constraints.get(cname))
    }

    return entry;
}
