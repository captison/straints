

let isolate = ruleStr =>
{
    // protect special parenthesized values
    ruleStr = ruleStr.replace(/\((\w+)\)/g, '<=[$1]=>');
    // isolate remaining parenthesis
    ruleStr = ruleStr.replace(/\s*(\(|\))\s*/g, ' $1 ');
    // reinstate protected values
    ruleStr = ruleStr.replace(/<=\[(\w+)\]=>/g, '($1)');
    // trim any excess whitespace
    ruleStr = ruleStr.trim();
    // split on whitespace for processing
    return ruleStr.split(/\s+/);
}

let parse = (ruleStr, resolver) =>
{
    return (success, failure) =>
    {
        let parts = [];
        // looping through non-whitespace elements of the rule expression
        utils.cycle(isolate(ruleStr), (next, item) =>
        {
            let results = C.RE.LOGIC.test(item) ? item : resolver(item);
            utils.async(results, i => { parts.push(i); next(); }, failure);
        },
        () => { success(parts.join(' ')); });
    }
}

let reduce = ruleStr =>
{
    ruleStr = '(' + ruleStr + ')';
    // revursively replace each parenthetical expression with its boolean result
    while (C.RE.GROUP.test(ruleStr))
    {
        ruleStr = ruleStr.replace(C.RE.GROUP, function(match, first)
        {
            // un-not the match as necessary by manually flipping booleans
            let unknot = first.replace(/not\s+true/ig, 'false').replace(/not\s+false/ig, 'true');
            // split it all by whitespace chars
            let parts = unknot.split(/\s+/);
            // get the first part is a boolean string
            let result = parts[0] === 'true' ? true : false;
            // every other part is a boolean string, so loop by twos
            for (let i=2,imx=parts.length;i<imx;i=i+2)
            {
                result = utils.booleate(parts[i-1], result, parts[i] === 'true' ? true : false);
            }
            return result;
        });
    }
    // rule string reduced to 'true' or 'false' at this point
    return ruleStr.trim() === 'true' ? true : false;
}

let evaluate = (ruleStr, resolver) =>
{
    return function(success, failure)
    {
        if (typeof ruleStr === 'string')
            utils.async(parse(ruleStr, resolver), rs => { success(reduce(rs)); }, failure);
        else
            failure(C.MSG.BAD_RULE_EXPRESSION, { value: ruleStr });
    }
}

export default evaluate;
