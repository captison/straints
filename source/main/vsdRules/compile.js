
/**
    Compiles parameters list for a constraint allowing for code evaluation
    (eval) and interpolation.

    @param {*} params
        The parameters to be compiled.
    @param {object} target
        Target pathing object.
    @return {*}
        Evaluated and interploated parameters.
 */
export default function(params, target)
{
    let compile = param =>
    {
        // if `param` is a string then interpolate it for replacements
        if (typeof param === 'string')
        {
            // evaluate `param` as target path
            if (C.RE.CP_PATH_REF.test(param))
                return target.get(param.slice(1)).data;
            // // evaluate `param` as regular expression
            else if (C.RE.CP_REGEX_REF.test(param))
                return eval(param);
        }
        // when `param` is an array use recursive resolution
        else if (Array.isArray(param))
        {
            return param.map(compile);
        }
        // resolve the value of every key when `param` is an object
        else if (utils.isObject(param))
        {
            return Object.keys(param).map(k => compile(param[k]));
        }

        return param;
    }

    return compile(params);
}
