
/**
    Results - The Straints validation result object.
 */
export default function(target)
{
    let results = { target, contexts: [], tested: {}, constraints: {}, tests: {}, isComplete: false, error: null };

    /**
        Returns a list of constraint IDs that resulted in [value] for
        [property] under the given validation [level].

        @param {string} [property]
            The property name to search. If the property sought is
            nested, this must be the fully qualified name. If not given,
            all properties are assumed.
        @param {string} [level=C.VSD.REQUIRE]
            The validation level.
        @param {boolean} [value=false]
            The result value to search. This can be null if you wish to
            find constraints that were not executed.
        @return {array}
            The list of constraint identifiers found.
     */
    results.findConstraints = (property, level = C.VSD.REQUIRE, value = false) =>
    {
        let names = [], tested = results.tested[level];

        if (tested)
        {
            (property ? [property] : Object.keys(tested)).forEach(prop =>
            {
                let cnames = tested[prop] || {};
                Object.keys(cnames).forEach(c => { if (cnames[c] === value) utils.push(c, names); });
            });
        }

        return names;
    };


    /**
        Returns a list of properties that resulted in [value] for
        [constraint] under the given validation [level].

        @param {string} [constraint]
            The name of the constraint to search. If not given, all
            constraints are assumed.
        @param {string} [level=C.VSD.REQUIRE]
            The validation level.
        @param {boolean} [value=false]
            The result value to search. This can be null if you wish to
            find properties with constraints that were not executed.
        @return {array}
            The list of properties found.
     */
    results.findProperties = (constraint, level = C.VSD.REQUIRE, value = false) =>
    {
        var props = [], tested = results.tested[level];

        if (tested)
        {
            Object.keys(tested).forEach(property =>
            {
                // get constraint results for the current property
                let item = tested[property];
                // checks a given contraint test on current property
                if (constraint)
                {
                    if (item[constraint] === value) props.push(property);
                }
                else
                {
                    for (var c in item)
                    {
                        if (item[c] === value) { props.push(property); break; }
                    }
                }
            });
        }

        return props;
    };

    /**
        For the given validation level, returns `true` if all tests passed,
        `false` if any failed, or `null` if none were run or if the level
        does not exist.

        @param {string} [level=C.VSD.REQUIRE]
            The validation level.
        @return {boolean}
            True if no errors found.
     */
     results.validFor = level => results.tested[level] ? results.findProperties(null, level).length === 0 : null


    /**
        Returns true if validation completed successfully, and there are
        no [C.VSD.REQUIRE] validation errors.

        @return {boolean}
            True if validation succeeded on all fronts.
     */
    results.valid = () => results.validFor(C.VSD.REQUIRE) !== false && !results.error


    /**
        Convenience method to return the payload for the specified constraint.

        @param {string} cname
            The constraint name.
        @return {*}
            Payload data for the constraint.
     */
    results.payload = cname => results.constraints[cname] && results.constraints[cname][C.VSD.PAYLOAD]


    return results;
}
